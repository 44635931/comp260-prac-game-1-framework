﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

public float maxSpeed = 5.0f; // in metres per second
public float acceleration = 3.0f; // in metres/second/second
    public float brake = 5.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float turnSpeed = 80.0f; // in degrees/second
    public float destroyRadius = 1.0f;
    void Update() {
        // the horizontal axis controls the turn
        float turn = Input.GetAxis("Horizontal");
        // turn the car

       // turnSpeed = Mathf.Clamp(turnSpeed, -300.0f, 300.0f);
       float actualTurnSpeed = turnSpeed - (speed * 10);
       // turnSpeed = 80 - (speed *10);
      
        transform.Rotate(0, 0, turn * -1 * actualTurnSpeed * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");

        if (forwards > 0) {
            // accelerate forwards
          //  actualTurnSpeed = 80 - (speed * 10);
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0) {
            // accelerate backwards
          //  actualTurnSpeed = 80 + (speed * 10);
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                speed = Mathf.Clamp(speed, 0, maxSpeed);
            }
            else if(speed < 0) 
            {
               // speed = 0;
                speed = speed + brake * Time.deltaTime;
                speed = Mathf.Clamp(speed, -maxSpeed, 0);
            }
        }
    

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);

        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }
    }

    private BeeSpawner beeSpawner;
    void Start()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }

}
